document.addEventListener('DOMContentLoaded', function() {
  // hard code the jql value
  // lots to learn about jql and its power. For the timeframe for this test I have taken the easy option.

  const params = {
    jql: 'project = FSDP AND issueType = "Service Request"'
  };

  //  ended up using the jira search endpoint as the /servicedeskapi/request endpoint only returned request information for the current user.

  AP.request({
    url: '/rest/api/3/search',
    type: 'POST',
    data: JSON.stringify(params),
    contentType: 'application/json',

    success: function(responseText) {
      const json = JSON.parse(responseText);
      const issues = json.issues;
      let reporters = [];

      // loop over all the issues and add information into an array.
      for (i = 0; i < issues.length; i++) {
        const currentIssue = issues[i];
        const currentReporterID = currentIssue.fields.creator.accountId;
        const currentReporterDisplayName =
          currentIssue.fields.creator.displayName;

        // item to push into array
        const item = {
          id: currentReporterID,
          displayName: currentReporterDisplayName,
          requests: 0
        };

        // only add id and display name once into the array of reporters
        if (
          !reporters.find(
            o =>
              o.id === currentReporterID &&
              o.displayName === currentReporterDisplayName
          )
        ) {
          reporters.push(item);
        }

        // find index based off reporter id
        const index = reporters.findIndex(o => o.id === currentReporterID);
        // increment requests in array for the current reporter
        reporters[index].requests++;
      }

      // pass reporters data into another funciton to handle updating the UI
      displayReporters(reporters);
    },
    error: function(err) {
      console.error(err, 'err');
      // With more time I would make sure the UI is updated to indicate an error has occurred
    }
  });

  // Simple function to update UI with reporters info
  function displayReporters(reporters) {
    const el = document.querySelector('#main-content');
    el.innerHTML = '';

    // loop over reporters array and display in the UI
    for (i = 0; i < reporters.length; i++) {
      const elChild = document.createElement('div');

      elChild.innerHTML =
        reporters[i].displayName +
        ' - Number of requests: ' +
        reporters[i].requests;

      el.appendChild(elChild);
    }
  }
});
